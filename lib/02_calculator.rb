def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array_of_nums)
  return array_of_nums.reduce(:+) if array_of_nums.reduce(:+)
  return 0
end

def multiply(array_of_nums)
  return array_of_nums.reduce(:*) if array_of_nums.reduce(:+)
  return 0
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  return 1 if num == 0
  (1..num).to_a.reduce(:*)
end
