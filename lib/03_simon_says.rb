def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times_repeated = 2)
  count = 1
  final_str = str
  until count == times_repeated
    final_str += " #{str}"
    count += 1
  end
  final_str
end
